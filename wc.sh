# count lines in each version of lineages, subtracted by 1 line (the header)

for i in $(\ls *.csv.gz); do
    echo -n "${i}   ";
    echo "$(zcat ${i} | wc -l) - 1" | bc
done
